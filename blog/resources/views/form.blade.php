<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Form Registration</title>
</head>

<body>

  <form action="/welcome" method="POST">
    @csrf
    <div class="container">
      <h1><b>Buat Akun Baru</b></h1>

      <p>
        <label>First Name:</label> <br>
        <input type="text" name="nama" />
      </p>
      <p>
        <label>Last Name:</label> <br>
        <input type="text" name="nama" />
      </p>
      <p>
        <label>Gender:</label> <br>
        <label><input type="radio" value="Male" /> Male</label><br>
        <label><input type="radio" value="Female" /> Female</label><br>
        <label><input type="radio" value="Other" /> Other</label><br>
      </p>
      <p>
        <label>Nationality:</label><br>
        <select name="nationality">
          <option value="Indonesia">Indonesia</option>
          <option value="Amerika">Amerika</option>
          <option value="Other">Other</option>
        </select>
      </p>
      <p>
        <label>Language Speoken:</label><br>
        <input type="checkbox" /> Bahasa Indonesia<br>
        <input type="checkbox" /> English<br>
        <input type="checkbox" /> Other<br>
      </p>

      <p>
        <label>Bio:</label><br>
        <textarea name="bio"></textarea>
      </p>
      <p>
        <input type="submit" name="submit" value="SignUp" />
      </p>
    </div>
  </form>

</body>

</html>